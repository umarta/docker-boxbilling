#/bin/sh

if getent passwd www > /dev/null 2>&1; then
   echo "user exits"
else
    groupadd -g 99999 www
    useradd -u 99999 -ms /bin/bash -g www www --password=123456 
fi
rm -rf boxbilling
wget https://github.com/boxbilling/boxbilling/releases/download/v4.22-beta.1/BoxBilling.zip
unzip BoxBilling.zip -d boxbilling
rm -rf boxbilling/install

cp ./config/bb-config.php ./boxbilling/bb-config.php
chown -R www:www boxbilling
rm BoxBilling.zip

rm -rf niagahoster-test

git clone https://gitlab.com/umarta/niagahoster-test.git
composer install --working-dir=niagahoster-test
cp ./niagahoster-test/.env.example ./niagahoster-test/.env
php niagahoster-test/artisan key:generate
RUN chown -R www:www ./niagahoster-test/storage
chown -R www:www niagahoster-test
chmod 777 -R ./niagahoster-test/storage/logs && chmod 777 -R ./niagahoster-test/storage

docker-compose up -d